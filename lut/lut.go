package lut

type Lut interface {
	LookupPoint(float64, float64, float64) (float64, float64, float64)
	GetLines() [][3]float64
	GetSize() uint32
	GetDimensions() int
	GetHash() string
	GetName() string
	GetDomain() ([]float64, []float64)
	WriteLines([][3]float64)
}
