package lut

import (
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"math"

	"gonum.org/v1/gonum/mat"
)

type ChannelLookupTable map[uint16]float64

type OneDimensionalLut struct {
	Name         string
	DomainMin    []float64
	DomainMax    []float64
	LutSize      *int
	TableEntries [3]ChannelLookupTable
}

func (lut *OneDimensionalLut) GetDomain() ([]float64, []float64) {
	return lut.DomainMin, lut.DomainMax
}

func (lut *OneDimensionalLut) GetName() string {
	return lut.Name
}

func (lut *OneDimensionalLut) GetHash() string {
	jsonBytes, _ := json.Marshal(lut.TableEntries)
	hash := sha512.Sum512(jsonBytes)
	return base64.StdEncoding.EncodeToString(hash[:])
}

func (lut *OneDimensionalLut) GetDimensions() int {
	return 1
}

func (lut *OneDimensionalLut) GetSize() uint32 {
	return uint32(*lut.LutSize)
}

func (lut *OneDimensionalLut) WriteLines(lines [][3]float64) {
	lut.TableEntries[0] = make(ChannelLookupTable, len(lines))
	for idx, line := range lines {
		lut.TableEntries[0][uint16(idx)] = line[0]
		lut.TableEntries[1][uint16(idx)] = line[1]
		lut.TableEntries[2][uint16(idx)] = line[2]
	}
}

func (lut *OneDimensionalLut) GetLines() [][3]float64 {
	res := make([][3]float64, *lut.LutSize)
	for i := uint16(0); i < uint16(*lut.LutSize); i++ {
		res = append(res, [3]float64{lut.TableEntries[0][i], lut.TableEntries[1][i], lut.TableEntries[2][i]})
	}
	return res
}

//LookupPoint takes the three dimensions of a point to be looked up, and returns the
//three dimensions of the lookup result
func (lut *OneDimensionalLut) LookupPoint(i, j, k float64) (float64, float64, float64) {
	iTable := lut.TableEntries[0]
	jTable := lut.TableEntries[1]
	kTable := lut.TableEntries[2]

	domainMin := mat.NewVecDense(3, lut.DomainMin)
	domainMax := mat.NewVecDense(3, lut.DomainMax)

	divisionSize := mat.NewVecDense(3, nil)
	divisionSize.SubVec(domainMax, domainMin)

	resI := applyChannel(i, iTable, domainMin.AtVec(0), divisionSize.AtVec(0))
	resJ := applyChannel(j, jTable, domainMin.AtVec(1), divisionSize.AtVec(1))
	resK := applyChannel(k, kTable, domainMin.AtVec(2), divisionSize.AtVec(2))

	return resI, resJ, resK
}

func applyChannel(val float64, table ChannelLookupTable, domainMin float64, divisionSize float64) float64 {
	isExact, floorIdx := findNearest(val, divisionSize, domainMin)
	if isExact {
		return table[floorIdx]
	} else {
		floorLoc := domainMin + float64(floorIdx)*divisionSize
		locationInInterval := (val - floorLoc) / divisionSize
		outputInterval := table[floorIdx+1] - table[floorIdx]
		return floorLoc + outputInterval*locationInInterval
	}
}

func findNearest(point float64, divisionSize float64, min float64) (bool, uint16) {
	relativeLoc := point - min
	idx := relativeLoc / divisionSize
	return (idx == math.Trunc(idx)), uint16(math.Trunc(idx))
}
