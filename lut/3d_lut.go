package lut

import (
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"math"

	"gonum.org/v1/gonum/mat"
)

type Point [3]float64
type Idx [3]uint16

type LookupTable map[Idx]Point

type ThreeDimensionalLut struct {
	Name         string
	DomainMin    []float64
	DomainMax    []float64
	LutSize      *int
	TableEntries LookupTable
}

func (lut *ThreeDimensionalLut) GetDomain() ([]float64, []float64) {
	return lut.DomainMin, lut.DomainMax
}

func (lut *ThreeDimensionalLut) GetName() string {
	return lut.Name
}

func (lut *ThreeDimensionalLut) GetHash() string {
	jsonBytes, _ := json.Marshal(lut.TableEntries)
	hash := sha512.Sum512(jsonBytes)
	return base64.StdEncoding.EncodeToString(hash[:])
}

func (lut *ThreeDimensionalLut) GetDimensions() int {
	return 3
}

func (lut *ThreeDimensionalLut) GetSize() uint32 {
	return uint32(*lut.LutSize)
}

func (lut *ThreeDimensionalLut) WriteLines(lines [][3]float64) {
	noLines := int(math.Pow(float64(*lut.LutSize), 3))
	keys := make([]Idx, noLines)
	for k := uint16(0); k < uint16(*lut.LutSize); k++ {
		for j := uint16(0); j < uint16(*lut.LutSize); j++ {
			for i := uint16(0); i < uint16(*lut.LutSize); i++ {
				keys = append(keys, Idx{i, j, k})
			}
		}
	}
	for idx, key := range keys {
		lut.TableEntries[key] = lines[idx]
	}
}

func (lut *ThreeDimensionalLut) GetLines() [][3]float64 {
	noLines := int(math.Pow(float64(*lut.LutSize), 3))
	res := make([][3]float64, noLines)
	keys := make([]Idx, noLines)
	for k := uint16(0); k < uint16(*lut.LutSize); k++ {
		for j := uint16(0); j < uint16(*lut.LutSize); j++ {
			for i := uint16(0); i < uint16(*lut.LutSize); i++ {
				keys = append(keys, Idx{i, j, k})
			}
		}
	}
	for _, key := range keys {
		res = append(res, lut.TableEntries[key])
	}
	return res
}

//Given the colour values for an RGB point (normalized to 0.0-1.0), returns the transformed colour values calculated with tetrahedral interpolation
func (lut *ThreeDimensionalLut) LookupPoint(i float64, j float64, k float64) (float64, float64, float64) {
	//Domain of the whole lookup table
	domainMin := mat.NewVecDense(3, lut.DomainMin[:])
	//domainMax := mat.NewVecDense(3, lut.DomainMax[:])
	//Point to look up
	point := mat.NewVecDense(3, []float64{i, j, k})
	//Point translated so that the domain minimum is at the origin
	dPoint := mat.NewVecDense(3, nil)
	dPoint.SubVec(point, domainMin)

	//fmt.Printf("Processing point (%v, %v, %v)\r\n", i, j, k)

	//The gap between the input values of each table entry
	divisionSizes := mat.NewDiagDense(3, []float64{
		float64(lut.DomainMax[0]-lut.DomainMin[0]) / float64(*lut.LutSize-1),
		float64(lut.DomainMax[1]-lut.DomainMin[1]) / float64(*lut.LutSize-1),
		float64(lut.DomainMax[2]-lut.DomainMin[2]) / float64(*lut.LutSize-1),
	})
	divisionInverse := mat.NewDense(3, 3, nil)
	divisionInverse.Inverse(divisionSizes)

	//Scaling of the point to be looked up so it is in the domain of indices
	pointIdxLoc := (mat.NewVecDense(3, nil))
	pointIdxLoc.MulVec(divisionInverse, dPoint)

	voxelMinIdxMat := mat.NewDense(3, 1, []float64{
		math.Floor(pointIdxLoc.AtVec(0)),
		math.Floor(pointIdxLoc.AtVec(1)),
		math.Floor(pointIdxLoc.AtVec(2)),
	})
	voxelMinIdxMat.Apply(
		func(i, _ int, val float64) float64 {
			if point.At(i, 0) == 1 {
				return val - 1
			} else {
				return val
			}
		}, voxelMinIdxMat)
	//Vectors containing the indices of the lower and upper corner of the cube in which the point to be looked up resides
	voxelMinIdx := voxelMinIdxMat.ColView(0)
	voxelMaxIdx := mat.NewVecDense(3, nil)
	voxelMaxIdx.AddVec(voxelMinIdx, mat.NewVecDense(3, []float64{1, 1, 1}))

	//If we can directly look up a colour val, we do not need to interpolate.
	if isInt(pointIdxLoc.AtVec(0)) && isInt(pointIdxLoc.AtVec(1)) && isInt(pointIdxLoc.AtVec(2)) {
		res := lut.TableEntries[Idx{uint16(pointIdxLoc.AtVec(0)), uint16(pointIdxLoc.AtVec(1)), uint16(pointIdxLoc.AtVec(2))}]
		//fmt.Printf("Got (%v, %v, %v)\r\n", res[0], res[1], res[2])
		return res[0], res[1], res[2]
	}

	//Otherwise choose which tetrahedron the point is in
	tetraVertsIdx := chooseTetra(dPoint.AtVec(0), dPoint.AtVec(1), dPoint.AtVec(2), voxelMinIdx, voxelMaxIdx, divisionSizes)

	//Calculate the total volume of the tetrahedron chosen
	totalVol := divisionSizes.At(0, 0) * divisionSizes.At(1, 1) * divisionSizes.At(2, 2) / 6

	//Calulate the volume of each sub-tetra and total the weighted lookups
	res := mat.NewVecDense(3, []float64{0, 0, 0})
	for idx := 0; idx < 4; idx++ {
		subTetra := mat.NewDense(3, 4, nil)
		subTetra.Mul(divisionSizes, tetraVertsIdx)
		subTetra.SetCol(idx, mat.Col(nil, 0, dPoint))
		weight := getTetraVol(subTetra) / totalVol
		lookUpVec := tetraVertsIdx.ColView(idx)
		lookUpVert := [3]uint16{uint16(lookUpVec.AtVec(0)), uint16(lookUpVec.AtVec(1)), uint16(lookUpVec.AtVec(2))}
		lookUpRes := [3]float64(lut.TableEntries[lookUpVert])
		resVec := mat.NewVecDense(3, lookUpRes[:])
		resVec.ScaleVec(weight, resVec)
		//fmt.Printf("Weight: %v, Val: %v\r\n", weight, resVec)

		res.AddVec(res, resVec)
	}
	//fmt.Printf("Got (%v, %v, %v).\r\n\r\n\r\n", res.AtVec(0), res.AtVec(1), res.AtVec(2))
	return res.AtVec(0), res.AtVec(1), res.AtVec(2)
}

//Given a matrix whose 4 columns each represent a vector containing the corners of a tetraheron,
//returns the total volume.
func getTetraVol(verts *mat.Dense) float64 {
	calcMatrix := mat.NewDense(4, 4, nil)
	calcMatrix.Augment(verts.T(), mat.NewVecDense(4, []float64{1, 1, 1, 1}))
	//fa := mat.Formatted(calcMatrix)
	//fmt.Printf("Tetra: \n%v\n", fa)
	return math.Abs(mat.Det(calcMatrix) / 6)
}

func isInt(i float64) bool {
	return i == math.Trunc(i)
}

func chooseTetra(i, j, k float64, min, max mat.Vector, divSizes mat.Matrix) *mat.Dense {
	di := i - min.AtVec(0)*divSizes.At(0, 0)
	dj := j - min.AtVec(1)*divSizes.At(1, 1)
	dk := k - min.AtVec(2)*divSizes.At(2, 2)

	//fmt.Printf("Using intravoxel posision of (%v, %v, %v)\r\n", di, dj, dk)

	var tetraVerts [4]*mat.VecDense

	if di >= dj && dj >= dk {
		//T1
		//fmt.Println("T1")
		tetraVerts = [4]*mat.VecDense{
			mat.NewVecDense(3, []float64{min.AtVec(0), min.AtVec(1), min.AtVec(2)}),
			mat.NewVecDense(3, []float64{max.AtVec(0), min.AtVec(1), min.AtVec(2)}),
			mat.NewVecDense(3, []float64{max.AtVec(0), max.AtVec(1), min.AtVec(2)}),
			mat.NewVecDense(3, []float64{max.AtVec(0), max.AtVec(1), max.AtVec(2)}),
		}
	} else if di >= dk && dk >= dj {
		//T2
		//fmt.Println("T2")
		tetraVerts = [4]*mat.VecDense{
			mat.NewVecDense(3, []float64{min.AtVec(0), min.AtVec(1), min.AtVec(2)}),
			mat.NewVecDense(3, []float64{max.AtVec(0), min.AtVec(1), min.AtVec(2)}),
			mat.NewVecDense(3, []float64{max.AtVec(0), min.AtVec(1), max.AtVec(2)}),
			mat.NewVecDense(3, []float64{max.AtVec(0), max.AtVec(1), max.AtVec(2)}),
		}
	} else if dk >= di && di >= dj {
		//T3
		//fmt.Println("T3")
		tetraVerts = [4]*mat.VecDense{
			mat.NewVecDense(3, []float64{min.AtVec(0), min.AtVec(1), min.AtVec(2)}),
			mat.NewVecDense(3, []float64{min.AtVec(0), min.AtVec(1), max.AtVec(2)}),
			mat.NewVecDense(3, []float64{max.AtVec(0), min.AtVec(1), max.AtVec(2)}),
			mat.NewVecDense(3, []float64{max.AtVec(0), max.AtVec(1), max.AtVec(2)}),
		}
	} else if dj >= di && di >= dk {
		//T4
		//fmt.Println("T4")
		tetraVerts = [4]*mat.VecDense{
			mat.NewVecDense(3, []float64{min.AtVec(0), min.AtVec(1), min.AtVec(2)}),
			mat.NewVecDense(3, []float64{min.AtVec(0), max.AtVec(1), min.AtVec(2)}),
			mat.NewVecDense(3, []float64{max.AtVec(0), max.AtVec(1), min.AtVec(2)}),
			mat.NewVecDense(3, []float64{max.AtVec(0), max.AtVec(1), max.AtVec(2)}),
		}
	} else if dj >= dk && dk >= di {
		//T5
		//fmt.Println("T5")
		tetraVerts = [4]*mat.VecDense{
			mat.NewVecDense(3, []float64{min.AtVec(0), min.AtVec(1), min.AtVec(2)}),
			mat.NewVecDense(3, []float64{min.AtVec(0), max.AtVec(1), min.AtVec(2)}),
			mat.NewVecDense(3, []float64{min.AtVec(0), max.AtVec(1), max.AtVec(2)}),
			mat.NewVecDense(3, []float64{max.AtVec(0), max.AtVec(1), max.AtVec(2)}),
		}
	} else {
		//T6
		//fmt.Println("T6")
		tetraVerts = [4]*mat.VecDense{
			mat.NewVecDense(3, []float64{min.AtVec(0), min.AtVec(1), min.AtVec(2)}),
			mat.NewVecDense(3, []float64{min.AtVec(0), min.AtVec(1), max.AtVec(2)}),
			mat.NewVecDense(3, []float64{min.AtVec(0), max.AtVec(1), max.AtVec(2)}),
			mat.NewVecDense(3, []float64{max.AtVec(0), max.AtVec(1), max.AtVec(2)}),
		}
	}
	res := mat.NewDense(3, 4, nil)
	for idx, vert := range tetraVerts {
		res.SetCol(idx, mat.Col(nil, 0, vert))
	}
	return res
}
