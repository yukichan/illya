package bitmap

import (
	"fmt"
	"image"
	"image/color"

	"gitlab.com/yukichan/illya/lut"
)

const SCALE_FACTOR = 0xffff

//ParseBitmapLut takes an image as well as a name for the LUT and the original filename and parses
//it into a lut struct. It will instead return an error if the horizontal and vertical dimensions do
//not match or if a non-RGBA colour model is used.
func ParseBitmapLut(inputLut image.Image, name string) (lut.Lut, error) {
	size := uint16(inputLut.Bounds().Dx())
	if int(size)*int(size) != inputLut.Bounds().Dy() {
		return nil, fmt.Errorf("bitmap was the wrong resolution for a cubic LUT")
	}
	if inputLut.ColorModel() != color.RGBAModel {
		return nil, fmt.Errorf("only RGBA lut bitmaps are supported at the current time")
	}

	table := make(map[lut.Idx]lut.Point)
	for r := uint16(0); r < size; r++ {
		for g := uint16(0); g < size; g++ {
			for b := uint16(0); b < size; b++ {
				x := inputLut.Bounds().Min.X + int((b*size)+r)
				y := inputLut.Bounds().Min.Y + int(g)
				idx := lut.Idx{r, g, b}
				rRes, gRes, bRes, _ := inputLut.At(x, y).RGBA()
				rScaled := float64(rRes) / SCALE_FACTOR
				gScaled := float64(gRes) / SCALE_FACTOR
				bScaled := float64(bRes) / SCALE_FACTOR
				point := lut.Point{rScaled, gScaled, bScaled}
				table[idx] = point
			}
		}
	}

	sizeInt := int(size)
	res := lut.ThreeDimensionalLut{
		Name:         name,
		DomainMin:    []float64{0, 0, 0},
		DomainMax:    []float64{1, 1, 1},
		LutSize:      &sizeInt,
		TableEntries: lut.LookupTable(table),
	}

	return &res, nil
}
