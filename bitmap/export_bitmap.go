package bitmap

import (
	"image"
	"image/color"

	"gitlab.com/yukichan/illya/lut"
)

const alpha = 0xFF

func ExportBitmap(lut lut.Lut, size int) image.Image {
	bounds := image.Rect(0, 0, size*size, size)
	res := image.NewRGBA(bounds)
	for r := 0; r < size; r++ {
		for g := 0; g < size; g++ {
			for b := 0; b < size; b++ {
				x := bounds.Min.X + int((b*size)+r)
				y := bounds.Min.Y + int(g)
				rScaled := float64(r) / float64(size-1)
				gScaled := float64(g) / float64(size-1)
				bScaled := float64(b) / float64(size-1)

				rRes, gRes, bRes := lut.LookupPoint(rScaled, gScaled, bScaled)
				res.SetRGBA(x, y, color.RGBA{uint8(rRes * 0xff), uint8(gRes * 0xff), uint8(bRes * 0xff), alpha})
			}
		}
	}
	return res
}
