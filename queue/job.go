package queue

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"unicode/utf8"

	"gitlab.com/yukichan/illya/apply"

	"gitlab.com/yukichan/illya/bitmap"

	"gitlab.com/yukichan/illya/cube"

	"gitlab.com/yukichan/illya/lut"
	"gitlab.com/yukichan/illya/store"
)

type JobStatusType int

const (
	Waiting = iota
	Failed
	Complete
	Unknown
)

type Job struct {
	ID        int
	JobType   string
	LutRef    *string
	InputFile []byte
	//Optional args depending on job type
	LutName string
	LutSize *int
}

type JobStatus struct {
	JobID   int
	Status  JobStatusType
	Result  []byte
	Message *string
}

const jpegQuality = 70

func processQueueItem(ctx context.Context, job Job, store store.Store) JobStatus {
	switch job.JobType {
	case "apply":
		if job.LutRef == nil {
			errorMsg := "no lutref supplied"
			status := JobStatus{JobID: job.ID, Status: Failed, Result: nil, Message: &errorMsg}
			return status
		}
		lut, err := store.ReadLut(ctx, *job.LutRef)
		if err != nil {
			errorMsg := fmt.Sprintf("Unable to fetch LUT from datastore with error %v", err)
			status := JobStatus{JobID: job.ID, Status: Failed, Result: nil, Message: &errorMsg}
			return status
		}
		if job.LutSize == nil {
			*job.LutSize = int(lut.GetSize())
		}
		r := bytes.NewReader(job.InputFile)
		img, _, err := image.Decode(r)
		if err != nil {
			errorMsg := fmt.Sprintf("Unable to decode image with error %v", err)
			status := JobStatus{JobID: job.ID, Status: Failed, Result: nil, Message: &errorMsg}
			return status
		}
		res := apply.ApplyToImage(lut, img)
		buf := new(bytes.Buffer)
		err = jpeg.Encode(buf, res, &jpeg.Options{Quality: jpegQuality})
		if err != nil {
			errorMsg := fmt.Sprintf("Unable to encode JPEG with error %v", err)
			status := JobStatus{JobID: job.ID, Status: Failed, Result: nil, Message: &errorMsg}
			return status
		}
		return JobStatus{
			JobID:   job.ID,
			Status:  Complete,
			Result:  buf.Bytes(),
			Message: nil,
		}
	case "topng":
		if job.LutRef == nil {
			errorMsg := "no lutref supplied"
			status := JobStatus{JobID: job.ID, Status: Failed, Result: nil, Message: &errorMsg}
			return status
		}
		lut, err := store.ReadLut(ctx, *job.LutRef)
		if err != nil {
			errorMsg := fmt.Sprintf("Unable to fetch LUT from datastore with error %v", err)
			status := JobStatus{JobID: job.ID, Status: Failed, Result: nil, Message: &errorMsg}
			return status
		}
		if job.LutSize == nil {
			*job.LutSize = int(lut.GetSize())
		}
		res := bitmap.ExportBitmap(lut, *job.LutSize)
		buf := new(bytes.Buffer)
		err = png.Encode(buf, res)
		if err != nil {
			errorMsg := fmt.Sprintf("Unable to encode PNG with error %v", err)
			status := JobStatus{JobID: job.ID, Status: Failed, Result: nil, Message: &errorMsg}
			return status
		}
		return JobStatus{
			JobID:   job.ID,
			Status:  Complete,
			Result:  buf.Bytes(),
			Message: nil,
		}
	case "tocube":
		errorMsg := fmt.Sprintf("Conversion to CUBE is not yet implemented")
		status := JobStatus{JobID: job.ID, Status: Failed, Result: nil, Message: &errorMsg}
		return status
	case "parse":
		lut, err := parseLut(job.InputFile, job.LutName)
		if err != nil {
			errorMsg := fmt.Sprintf("Lut parsing failed with error %v", err)
			return JobStatus{JobID: job.ID, Status: Failed, Result: nil, Message: &errorMsg}
		}
		hash, err := store.WriteLut(ctx, lut)
		if err != nil {
			errorMsg := fmt.Sprintf("Failed to write LUT to the datastore with error %v", err)
			return JobStatus{JobID: job.ID, Status: Failed, Result: nil, Message: &errorMsg}
		}
		return JobStatus{JobID: job.ID, Status: Complete, Result: []byte(hash), Message: nil}
	default:
		errorMsg := fmt.Sprintf("Job type %v was not recognised.", job.JobType)
		return JobStatus{JobID: job.ID, Status: Failed, Result: nil, Message: &errorMsg}
	}
}

func parseLut(lut []byte, name string) (lut.Lut, error) {
	r := bytes.NewReader(lut)
	if utf8.Valid(lut) {
		//Input file is a valid text file, so we can probably assume it is a cube file
		scanner := bufio.NewScanner(r)
		return cube.ParseCubeFile(scanner, name)
	} else {
		//We can probably assume it is a PNG file.
		//TODO: actually test this.
		img, _, err := image.Decode(r)
		if err != nil {
			return nil, err
		}
		return bitmap.ParseBitmapLut(img, name)
	}
}
