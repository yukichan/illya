package queue

import (
	"context"
	"fmt"
	"time"

	"github.com/gomodule/redigo/redis"
	"github.com/sirupsen/logrus"
	"gitlab.com/yukichan/illya/store"
)

const QUEUENAME = "jobs"
const QUEUENAMESPACE = "job"
const feedbackChannel = "jobstatus"
const ERRORDELAY = 30 * time.Second
const jobTimeout = 20 * time.Second

func ProcessQueueRedis(addr string, dbNo int, storeRef store.Store) error {
	conn, err := redis.DialURL(addr, redis.DialDatabase(dbNo))
	if err != nil {
		return err
	}
	logrus.Info("Successfully connected to Redis queue source")
	defer conn.Close()
	//Loop forever, retrieving jobs from job queue
	logrus.Debug("Now listening for jobs")
	for {
		logrus.Debug("Waiting for a job")
		jobID, err := redis.String(conn.Do("BLPOP", QUEUENAME, 0))
		if err != nil {
			logrus.Errorf("Failed to retrieve job with error %v", err)
			time.Sleep(ERRORDELAY)
			continue
		}
		logrus.Infof("Got job ID %v from Redis queue, now fetching details", jobID)
		var job Job
		ctx, cancel := context.WithTimeout(context.Background(), jobTimeout)
		jobKey := fmt.Sprintf("%s:%s", QUEUENAMESPACE, jobID)
		value, err := redis.Values(conn.Do("HGETALL", jobKey))
		if err != nil {
			logrus.Errorf("Failed to retrieve details for job %v with errror %v", jobID, err)
			time.Sleep(ERRORDELAY)
			continue
		}
		redis.ScanStruct(value, &job)
		logrus.Info("Fetched job from Redis")
		status := processQueueItem(ctx, job, storeRef)
		logrus.Info("Job completed.")
		if status.Status == Failed {
			logrus.Warnf("Job %v failed with message %v", status.JobID, status.Message)
		}
		logrus.Debug("Now pushing job result to listeners")
		_, err = conn.Do("PUBLISH", feedbackChannel, status)
		if err != nil {
			logrus.Errorf("Failed to push job status to listeners with error %v", err)
		}
		cancel()
	}
}
