#Use the official golang alpine image
FROM golang:1.11-alpine

#Install git to fetch packages
RUN apk update && apk upgrade && apk add --no-cache bash git openssh

#Get the most recent version from gitlab and fetch deps
RUN go get -u gitlab.com/yukichan/illya

#Overwrite with any updates on the local copy
ADD . /go/src/gitlab.com/yukichan/illya

#Get go deps
RUN go get /go/src/gitlab.com/yukichan/illya

#Compile and install binary
RUN go install -i gitlab.com/yukichan/illya


ENTRYPOINT ["/go/bin/illya"]
