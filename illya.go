package main

import (
	"bufio"
	"flag"
	"fmt"
	"image"
	"image/png"
	"os"
	"strings"

	"github.com/sirupsen/logrus"

	"gitlab.com/yukichan/illya/apply"
	"gitlab.com/yukichan/illya/lut"
	"gitlab.com/yukichan/illya/queue"

	"gitlab.com/yukichan/illya/bitmap"
	"gitlab.com/yukichan/illya/cube"
	"gitlab.com/yukichan/illya/store"
)

var (
	jobQueueSource string
	tableCache     string
	queueAddr      string
	cacheAddr      string
	cacheDb        string
	outputFile     string
	jobQueueDb     int
	lutSize        int
	verbose        bool
	superVerbose   bool
)

func init() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options] [verb [lutFile] [inputImageFile]]\n", os.Args[0])
		fmt.Fprintln(os.Stderr, "Supported verbs are: apply, topng, tocube.")
		flag.PrintDefaults()
	}
	flag.StringVar(&jobQueueSource, "jobsource", "None", "The source from which jobs can be fetched. Can be and of 'Redis' or 'None'")
	flag.StringVar(&tableCache, "tablecache", "None", "The location in which parsed lookup tables should be stored. Can be any of 'Mongo' or 'None'. Only used if 'jobsource' is not None")
	flag.StringVar(&cacheAddr, "tablecacheaddr", "mongo://localhost:27017", "The address of the database used to store parsed LUTs (if any). Defaults to localhost on port 27017")
	flag.StringVar(&cacheDb, "tablecachedb", "Miyu", "The name of the db used to store parsed LUTs (if any). Defaults to Illya.")
	flag.StringVar(&outputFile, "o", "a.out", "The file to output to. Only used if 'jobsource' is None")
	flag.StringVar(&queueAddr, "queueaddr", "redis://localhost:6379", "The address of the queue provider instance that should be used (if any). Defaults to localhost on port 6379")
	flag.IntVar(&lutSize, "lutsize", 32, "The default size for the converted lookup table (default 32)")
	flag.IntVar(&jobQueueDb, "redisqueuedb", 0, "The redis database number that should be used for the queue, if any. Defaults to 0")
	flag.BoolVar(&verbose, "v", false, "Increases the verbosity level.")
	flag.BoolVar(&superVerbose, "vv", false, "Increases the verbosity level even further.")
}

func main() {
	flag.Parse()
	args := flag.Args()

	if superVerbose {
		logrus.SetLevel(logrus.TraceLevel)
	} else if verbose {
		logrus.SetLevel(logrus.InfoLevel)
	} else {
		logrus.SetLevel(logrus.ErrorLevel)
	}

	switch jobQueueSource {
	case "None":
		//We should read the input cube file, verb and optionally source and destination images from the command line
		logrus.Infoln("Running in single-run mode")
		runOnce(args)
	case "Redis":
		//We should monitor a redis store for job data.
		logrus.Infoln("Running in queued mode with a Redis jobsource")
		runQueuedRedis()
	default:
		//Unknown queue source
		logrus.Fatalf("Unknown queue source %v. Now terminating.\n", jobQueueSource)
	}
}

func runOnce(args []string) {
	if len(args) == 0 {
		logrus.Fatalln("You need to provide a verb. Use the -h or --help flags for available options.")
	}
	verb := args[0]
	switch verb {
	case "apply":
		//We want to apply a lut to an image
		if len(args) != 3 {
			logrus.Fatalln("Syntax for the apply command is 'illya [options] apply lut_file input_image'")
		}
		lut, err := loadLut(args[1])
		if err != nil {
			logrus.Fatalln(err)
		}
		reader, err := os.Open(args[2])
		if err != nil {
			logrus.Fatalf("Could not load input image file due to error %v\n", err)
		}
		defer reader.Close()
		writer, err := os.Create(outputFile)
		if err != nil {
			logrus.Fatalf("Could not open file for writing due to error %v\n", err)
		}
		defer writer.Close()
		img, _, err := image.Decode(reader)
		if err != nil {
			logrus.Fatalln("Could not read provided image file.")
		}
		applied := apply.ApplyToImage(lut, img)
		png.Encode(writer, applied)
	case "topng":
		//We want to convert a lut to a png file for use with reshade
		if len(args) != 2 {
			logrus.Fatalln("Syntax for the topng command is 'illya [options] topng [lut_file]'")
		}
		lut, err := loadLut(args[1])
		if err != nil {
			logrus.Fatalln(err)
		}
		writer, err := os.Create(outputFile)
		if err != nil {
			logrus.Fatalf("Could not open file for writing due to error %v", err)
		}
		defer writer.Close()
		res := bitmap.ExportBitmap(lut, lutSize)
		png.Encode(writer, res)
	case "tocube":
		//We want to convert a lut to a standard cube file
		logrus.Fatalln("Conversion to cube files is currently unsupported.")
	}
}

func runQueuedRedis() {
	var s store.Store
	var err error
	switch tableCache {
	case "Mongo":
		logrus.Info("Using Mongo as table cache")
		s, err = store.InitMongoStore(cacheAddr, cacheDb)
		if err != nil {
			logrus.Fatalf("Failed to initialize Mongo connection with error %v", err)
		}
		logrus.Info("Successfully connected to Mongo store")
	default:
		logrus.Fatalf("Store %v is not currently supported. Please choose either 'Mongo' or 'None'.", tableCache)
	}
	err = queue.ProcessQueueRedis(queueAddr, jobQueueDb, s)
	if err != nil {
		logrus.Fatalf("Job queue failed with error %v", err)
	}
}

func loadLut(path string) (lut.Lut, error) {
	res, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer res.Close()

	splitFileName := strings.Split(path, ".")
	extension := splitFileName[len(splitFileName)-1]
	switch extension {
	case "cube":
		//parse as cube file
		logrus.Info("Parsing as Adobe CUBE LUT")
		scanner := bufio.NewScanner(res)
		return cube.ParseCubeFile(scanner, "")
	case "png":
		//parse as png
		logrus.Info("Parsing as Reshade-compatible PNG")
		img, _, err := image.Decode(res)
		if err != nil {
			return nil, err
		}
		return bitmap.ParseBitmapLut(img, fmt.Sprintf("Illya.%v", path))
	default:
		logrus.Fatalf("Files with the extension %s are not currently supported.\n", extension)
	}
	return nil, nil
}
