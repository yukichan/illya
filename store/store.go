package store

import (
	"context"

	"gitlab.com/yukichan/illya/lut"
)

//Store is an interface for classes that implement a way to store and retrieve LUTs in the internal format.
type Store interface {
	WriteLut(ctx context.Context, lut lut.Lut) (string, error)
	ReadLut(ctx context.Context, identifier string) (lut.Lut, error)
}
