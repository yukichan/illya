package store

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/yukichan/illya/lut"
	"gopkg.in/mgo.v2/bson"

	"github.com/mongodb/mongo-go-driver/mongo"
)

const connectTimeout = 20 * time.Second
const collectionName = "luts"

//MongoStore is an implementation of LUT store using a MongoDB backend
type MongoStore struct {
	client *mongo.Client
	dbAddr string
	db     string
}

func InitMongoStore(addr, db string) (*MongoStore, error) {
	var store MongoStore
	store.dbAddr = addr
	store.db = db
	client, err := mongo.NewClient(addr)
	if err != nil {
		return nil, err
	}
	ctx, cancel := context.WithTimeout(context.Background(), connectTimeout)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}
	store.client = client
	return &store, nil
}

func (store *MongoStore) WriteLut(ctx context.Context, lut lut.Lut) (string, error) {
	collection := store.client.Database(store.db).Collection(collectionName)
	var storedTable map[string]interface{}
	storedTable["name"] = lut.GetName()
	storedTable["domain_min"], storedTable["domain_max"] = lut.GetDomain()
	storedTable["lut_size"] = lut.GetSize()
	storedTable["table"] = lut.GetLines()
	storedTable["dimensions"] = lut.GetDimensions()
	hash := lut.GetHash()
	storedTable["hash"] = hash
	_, err := collection.InsertOne(ctx, storedTable)
	return hash, err
}

func (store *MongoStore) ReadLut(ctx context.Context, identifier string) (lut.Lut, error) {
	collection := store.client.Database(store.db).Collection(collectionName)
	result := collection.FindOne(ctx, bson.M{"hash": identifier})
	var table map[string]interface{}
	err := result.Decode(&table)
	if err != nil {
		return nil, err
	}
	size := table["lut_size"].(int)
	if table["dimensions"].(int) == 3 {
		res := lut.ThreeDimensionalLut{
			Name:      table["name"].(string),
			DomainMin: table["domain_min"].([]float64),
			DomainMax: table["domain_max"].([]float64),
			LutSize:   &size,
		}
		(&res).WriteLines(table["table"].([][3]float64))
		return &res, nil
	} else if table["dimensions"].(int) == 1 {
		res := lut.OneDimensionalLut{
			Name:      table["name"].(string),
			DomainMin: table["domain_min"].([]float64),
			DomainMax: table["domain_max"].([]float64),
			LutSize:   &size,
		}
		(&res).WriteLines(table["table"].([][3]float64))
		return &res, nil
	} else {
		return nil, fmt.Errorf("Invalid dimension: %v", table["lut_size"])
	}
}
