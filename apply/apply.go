package apply

import (
	"image"
	"image/color"

	"gitlab.com/yukichan/illya/lut"
)

func ApplyToImage(lut lut.Lut, input image.Image) *image.RGBA {
	res := image.NewRGBA(input.Bounds())
	b := input.Bounds()
	for y := b.Min.Y; y < b.Max.Y; y++ {
		for x := b.Min.X; x < b.Max.X; x++ {
			startingColour := input.At(x, y)
			rOrig, gOrig, bOrig, a := startingColour.RGBA()
			rScaled := float64(rOrig) / 0xffff
			gScaled := float64(gOrig) / 0xffff
			bScaled := float64(bOrig) / 0xffff
			r, g, b := lut.LookupPoint(rScaled, gScaled, bScaled)
			res.SetRGBA(x, y, color.RGBA{uint8(r * 0xff), uint8(g * 0xff), uint8(b * 0xff), uint8(a)})
		}
	}
	return res
}
