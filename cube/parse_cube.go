package cube

import (
	"bufio"
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/yukichan/illya/lut"
)

type cubeLut struct {
	name       *string
	domainMin  []float64
	domainMax  []float64
	oneDSize   *uint16
	threeDSize *uint8
	table      [][]float64
}

func ParseCubeFile(inputLut *bufio.Scanner, name string) (lut.Lut, error) {
	res := cubeLut{name: nil, domainMin: []float64{0, 0, 0}, domainMax: []float64{1, 1, 1}, oneDSize: nil, threeDSize: nil, table: make([][]float64, 0)}
	for inputLut.Scan() {
		err := parseCubeLine(inputLut.Text(), &res)
		if err != nil {
			return nil, err
		}
	}
	return exportCubeLut(res, name)
}

func exportCubeLut(input cubeLut, name string) (lut.Lut, error) {
	if input.oneDSize == nil && input.threeDSize == nil {
		log.Printf("LUT had neither LUT_1D_SIZE nor LUT_3D_SIZE defined")
		return nil, fmt.Errorf("LUT had neither LUT_1D_SIZE nor LUT_3D_SIZE defined, so parsing cannot continue")
	} else if input.oneDSize == nil {
		return export3dLut(input, name)
	} else if input.threeDSize == nil {
		return export1dLut(input, name)
	}
	log.Printf("LUT had neither LUT_1D_SIZE nor LUT_3D_SIZE defined")
	return nil, fmt.Errorf("LUT had neither LUT_1D_SIZE nor LUT_3D_SIZE defined, so parsing cannot continue")
}

func export1dLut(input cubeLut, name string) (*lut.OneDimensionalLut, error) {
	lutSize := int(*input.oneDSize)
	if name == "" && input.name != nil {
		name = *input.name
	} else {
		name = ""
	}

	res := lut.OneDimensionalLut{
		Name:      name,
		DomainMin: input.domainMin,
		DomainMax: input.domainMax,
		LutSize:   &lutSize,
	}
	domainSizeI := res.DomainMax[0] - res.DomainMin[0]
	domainSizeJ := res.DomainMax[1] - res.DomainMin[1]
	domainSizeK := res.DomainMax[2] - res.DomainMin[2]
	if domainSizeI < 0 || domainSizeJ < 0 || domainSizeK < 0 {
		log.Printf("LUT has negative domain size in one or more dimensions. This is not allowed by the Adobe cube spec")
		return nil, fmt.Errorf("LUT has negative domain size")
	}

	res.TableEntries[0] = make(map[uint16]float64, 0)
	res.TableEntries[1] = make(map[uint16]float64, 0)
	res.TableEntries[2] = make(map[uint16]float64, 0)

	for i, entry := range input.table {
		res.TableEntries[0][uint16(i)] = entry[0]
		res.TableEntries[1][uint16(i)] = entry[1]
		res.TableEntries[2][uint16(i)] = entry[2]
	}

	return &res, nil
}

func export3dLut(input cubeLut, name string) (*lut.ThreeDimensionalLut, error) {
	lutSize := int(*input.threeDSize)
	if name == "" && input.name != nil {
		name = *input.name
	} else {
		name = ""
	}

	res := lut.ThreeDimensionalLut{
		Name:         name,
		DomainMin:    input.domainMin,
		DomainMax:    input.domainMax,
		LutSize:      &lutSize,
		TableEntries: make(map[lut.Idx]lut.Point),
	}
	domainSizeI := res.DomainMax[0] - res.DomainMin[0]
	domainSizeJ := res.DomainMax[1] - res.DomainMin[1]
	domainSizeK := res.DomainMax[2] - res.DomainMin[2]
	if domainSizeI < 0 || domainSizeJ < 0 || domainSizeK < 0 {
		log.Printf("LUT has negative domain size in one or more dimensions. This is not allowed by the Adobe cube spec")
		return nil, fmt.Errorf("LUT has negative domain size")
	}
	for k := 0; k < lutSize; k++ {
		for j := 0; j < lutSize; j++ {
			for i := 0; i < lutSize; i++ {
				entry := input.table[i+lutSize*(j+lutSize*k)]
				inputPoint := lut.Idx{uint16(i), uint16(j), uint16(k)}
				outputPoint := lut.Point{entry[0], entry[1], entry[2]}
				res.TableEntries[inputPoint] = outputPoint
			}
		}
	}

	return &res, nil
}

func parseCubeLine(line string, res *cubeLut) error {
	if line == "" {
		//line is empty, so move on
		return nil
	}

	if line[0] == '#' {
		//line is a comment, so move on
		return nil
	}

	words := strings.Split(line, " ")
	switch words[0] {
	case "TITLE":
		if len(words) < 2 {
			log.Printf("Lut table title specification has the wrong number of terms on line '%#v'", line)
			return fmt.Errorf("lut table title specification has the wrong number of terms on line %#v", line)
		}
		restOfLine := strings.Join(words[1:], " ")
		startIndex := strings.Index(restOfLine, "\"") + 1
		endIndex := strings.LastIndex(restOfLine, "\"")
		name := restOfLine[startIndex:endIndex]
		res.name = &name
	case "DOMAIN_MIN":
		if len(words) != 4 {
			log.Printf("Lut table domain specification has the wrong number of terms on line %v", line)
			return fmt.Errorf("lut table domain specification has the wrong number of terms on line v")
		}
		red, err := strconv.ParseFloat(words[1], 64)
		if err != nil {
			log.Printf("Failed to parse Red value with error %v", err)
			return err
		}
		green, err := strconv.ParseFloat(words[2], 64)
		if err != nil {
			log.Printf("Failed to parse Green value with error %v", err)
			return err
		}
		blue, err := strconv.ParseFloat(words[3], 64)
		if err != nil {
			log.Printf("Failed to parse Blue value with error %v", err)
			return err
		}
		res.domainMin = []float64{red, green, blue}
		return nil
	case "DOMAIN_MAX":
		if len(words) != 4 {
			log.Printf("Lut table domain specification has the wrong number of terms on line %v", line)
			return fmt.Errorf("lut table domain specification has the wrong number of terms on line v")
		}
		red, err := strconv.ParseFloat(words[1], 64)
		if err != nil {
			log.Printf("Failed to parse Red value with error %v", err)
			return err
		}
		green, err := strconv.ParseFloat(words[2], 64)
		if err != nil {
			log.Printf("Failed to parse Green value with error %v", err)
			return err
		}
		blue, err := strconv.ParseFloat(words[3], 64)
		if err != nil {
			log.Printf("Failed to parse Blue value with error %v", err)
			return err
		}
		res.domainMax = []float64{red, green, blue}
		return nil
	case "LUT_1D_SIZE":
		if len(words) != 2 {
			log.Printf("Lut table size has the wrong number of terms on line %v", line)
			return fmt.Errorf("lut table size has the wrong number of terms on line v")
		}
		size, err := strconv.ParseInt(words[1], 0, 32)
		if err != nil {
			log.Printf("Failed to parse LUT size with error %v", err)
			return err
		}
		size16 := uint16(size)
		res.oneDSize = &size16
		return nil
	case "LUT_3D_SIZE":
		if len(words) != 2 {
			log.Printf("Lut table size has the wrong number of terms on line %v", line)
			return fmt.Errorf("lut table size has the wrong number of terms on line v")
		}
		size, err := strconv.ParseInt(words[1], 0, 16)
		if err != nil {
			log.Printf("Failed to parse LUT size with error %v", err)
			return err
		}
		size8 := uint8(size)
		res.threeDSize = &size8
		return nil
	default:
		//We can assume that this is a table value
		if len(words) != 3 {
			log.Printf("Lut table entry has the wrong number of terms on line %v", line)
			return fmt.Errorf("lut table entry has the wrong number of terms on line v")
		}
		red, err := strconv.ParseFloat(words[0], 64)
		if err != nil {
			log.Printf("Failed to parse red channel on line %v with error %v", line, err)
			return err
		}
		green, err := strconv.ParseFloat(words[1], 64)
		if err != nil {
			log.Printf("Failed to parse green channel on line %v with error %v", line, err)
			return err
		}
		blue, err := strconv.ParseFloat(words[2], 64)
		if err != nil {
			log.Printf("Failed to parse blue channel on line %v with error %v", line, err)
			return err
		}
		res.table = append(res.table, []float64{red, green, blue})
	}
	return nil
}
